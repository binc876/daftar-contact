@extends('layout/main')

@section('title', 'Contact')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class="mt-3">Daftar Kontak</h1>
                
                <table class="table">
                    <thead class="table-dark">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama</th>
                            <th scope="col">No Telepon</th>
                            <th scope="col">Alamat</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach( $contact as $cnt )
                        <tr>
                            <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{ $cnt->Nama }}</td>
                            <td>{{ $cnt->Telepon }}</td>
                            <td>{{ $cnt->Alamat }}</td>
                            <td>
                                <a href="" class="badge bg-success">edit</a>
                                <a href="" class="badge bg-danger">delete</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection